﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Threading;
using System.Diagnostics;

namespace Sinal_Especialistas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //DMX
        public ArtNet.Engine artEngine = new ArtNet.Engine("", "");
        public byte[] DMXData = new byte[512];
        public short universe = 0;

        List<int> gLstCanais = new List<int>();

        //SOCKET RECEBIMENTO
        const int Porta_Recebimento = 9000;
        public TcpListener tcpRecebimento = new TcpListener(Porta_Recebimento);

        public Thread gThreadRecebimento;
        public string gResposta = "";

        public DispatcherTimer tmrAtualizar = new DispatcherTimer(DispatcherPriority.Render);

        BitmapImage gVerde = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\VERDE.png"));
        BitmapImage gVermelho = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\VERMELHO.png"));
        BitmapImage gPreto = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\PRETO.png"));

        public MainWindow()
        {
            InitializeComponent();

            CarregarConfiguracoes();
            CarregarImagensParticipantes();

            tmrAtualizar.Interval = TimeSpan.FromMilliseconds(1);
            EventHandler handlerAtualizar = new EventHandler(tmrAtualizar_TICK);
            tmrAtualizar.Tick += handlerAtualizar;

            gThreadRecebimento = new Thread(this.ReceberInformacoesViaSocket);
            gThreadRecebimento.SetApartmentState(ApartmentState.MTA);
            gThreadRecebimento.Start();
        }

        private void tmrAtualizar_TICK(object o, EventArgs sender)
        {
            try
            {
                if (gResposta == "RESET")
                {
                    btnEspecialista01.Source = gVermelho;
                    btnEspecialista01.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista01);

                    btnEspecialista02.Source = gVermelho;
                    btnEspecialista02.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista02);

                    btnEspecialista03.Source = gVermelho;
                    btnEspecialista03.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista03);

                    btnEspecialista04.Source = gVermelho;
                    btnEspecialista04.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista04);

                    btnEspecialista05.Source = gVermelho;
                    btnEspecialista05.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista05);

                    btnEspecialista06.Source = gVermelho;
                    btnEspecialista06.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista06);

                    btnEspecialista07.Source = gVermelho;
                    btnEspecialista07.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista07);

                    btnEspecialista08.Source = gVermelho;
                    btnEspecialista08.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista08);

                    btnEspecialista09.Source = gVermelho;
                    btnEspecialista09.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista09);

                    btnEspecialista10.Source = gVermelho;
                    btnEspecialista10.Tag = "FALSE";
                    Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                }
                else
                {
                    string[] words = gResposta.Split('|');

                    if ((words[0] == "SIM"))
                    {
                        //btnEspecialista01.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista01.Source = gVermelho;
                        btnEspecialista01.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista01);
                    }

                    if ((words[1] == "SIM"))
                    {
                        //btnEspecialista02.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista02.Source = gVermelho;
                        btnEspecialista02.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista02);
                    }

                    if ((words[2] == "SIM"))
                    {
                        //btnEspecialista03.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista03.Source = gVermelho;
                        btnEspecialista03.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista03);
                    }

                    if ((words[3] == "SIM"))
                    {
                        //btnEspecialista04.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista04.Source = gVermelho;
                        btnEspecialista04.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista04);
                    }

                    if ((words[4] == "SIM"))
                    {
                        //btnEspecialista05.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista05.Source = gVermelho;
                        btnEspecialista05.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista05);
                    }

                    if ((words[5] == "SIM"))
                    {
                        //btnEspecialista06.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista06.Source = gVermelho;
                        btnEspecialista06.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista06);
                    }

                    if ((words[6] == "SIM"))
                    {
                        //btnEspecialista07.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista07.Source = gVermelho;
                        btnEspecialista07.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista07);
                    }

                    if ((words[7] == "SIM"))
                    {
                        //btnEspecialista08.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista08.Source = gVermelho;
                        btnEspecialista08.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista08);
                    }

                    if ((words[8] == "SIM"))
                    {
                        //btnEspecialista09.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista09.Source = gVermelho;
                        btnEspecialista09.Tag = "FALSE";
                        Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista09);
                    }

                    if ((words[9] == "SIM"))
                    {
                        //btnEspecialista10.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                        btnEspecialista10.Source = gVermelho;
                        btnEspecialista10.Tag = "FALSE";
                        Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                    }
                }
                tmrAtualizar.Stop();
            }
            catch (Exception E)
            {

            }
        }

        private void ReceberInformacoesViaSocket()
        {
            try
            {
                while (2 > 1)
                {
                    tcpRecebimento.Start();

                    TcpClient tcpClient = tcpRecebimento.AcceptTcpClient();

                    NetworkStream stream = tcpClient.GetStream();
                    byte[] bytes = new byte[tcpClient.ReceiveBufferSize + 1];

                    int vTamanho = stream.Read(bytes, 0, bytes.Length);
                    dynamic vResposta = Encoding.UTF8.GetString(bytes, 0, vTamanho);

                    gResposta = vResposta;

                    tcpRecebimento.Stop();
                    tmrAtualizar.Start();

                    Thread.Sleep(100);

                    tcpClient.Close();
                    tcpRecebimento.Stop();
                }

                //ReceberInformacoesViaSocket();
            }
            catch (Exception e)
            {
                tcpRecebimento.Stop();
                ReceberInformacoesViaSocket();
            }
        }

        private void btnSalvar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Properties.Settings.Default.canalEspecialista01 = txtCanal01.Text;
                Properties.Settings.Default.canalEspecialista02 = txtCanal02.Text;
                Properties.Settings.Default.canalEspecialista03 = txtCanal03.Text;
                Properties.Settings.Default.canalEspecialista04 = txtCanal04.Text;
                Properties.Settings.Default.canalEspecialista05 = txtCanal05.Text;
                Properties.Settings.Default.canalEspecialista06 = txtCanal06.Text;
                Properties.Settings.Default.canalEspecialista07 = txtCanal07.Text;
                Properties.Settings.Default.canalEspecialista08 = txtCanal08.Text;
                Properties.Settings.Default.canalEspecialista09 = txtCanal09.Text;
                Properties.Settings.Default.canalEspecialista10 = txtCanal10.Text;

                Properties.Settings.Default.canalDirecao = txtCanalDirecao.Text;

                Properties.Settings.Default.corBloqueada = clpkCorBloqueado.SelectedColor;
                Properties.Settings.Default.corPermitida = clpkCorPermitido.SelectedColor;

                Properties.Settings.Default.Save();
            }
            catch (Exception E)
            {

            }
        }

        private void CarregarConfiguracoes()
        {
            try
            {
                txtCanal01.Text = Properties.Settings.Default.canalEspecialista01;
                txtCanal02.Text = Properties.Settings.Default.canalEspecialista02;
                txtCanal03.Text = Properties.Settings.Default.canalEspecialista03;
                txtCanal04.Text = Properties.Settings.Default.canalEspecialista04;
                txtCanal05.Text = Properties.Settings.Default.canalEspecialista05;
                txtCanal06.Text = Properties.Settings.Default.canalEspecialista06;
                txtCanal07.Text = Properties.Settings.Default.canalEspecialista07;
                txtCanal08.Text = Properties.Settings.Default.canalEspecialista08;
                txtCanal09.Text = Properties.Settings.Default.canalEspecialista09;
                txtCanal10.Text = Properties.Settings.Default.canalEspecialista10;

                clpkCorBloqueado.SelectedColor = Properties.Settings.Default.corBloqueada;
                clpkCorPermitido.SelectedColor = Properties.Settings.Default.corPermitida;

                gLstCanais.Clear();

                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista01));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista02));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista03));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista04));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista05));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista06));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista07));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista08));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista09));
                gLstCanais.Add(Convert.ToInt16(Properties.Settings.Default.canalEspecialista10));

                txtCanalDirecao.Text = Properties.Settings.Default.canalDirecao;

                btnEspecialista01.Source = gVermelho;
                btnEspecialista02.Source = gVermelho;
                btnEspecialista03.Source = gVermelho;
                btnEspecialista04.Source = gVermelho;
                btnEspecialista05.Source = gVermelho;
                btnEspecialista06.Source = gVermelho;
                btnEspecialista07.Source = gVermelho;
                btnEspecialista08.Source = gVermelho;
                btnEspecialista09.Source = gVermelho;
                btnEspecialista10.Source = gVermelho;                

                //btnEspecialista01.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista02.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista03.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista04.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista05.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista06.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista07.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista08.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista09.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista10.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
            }
            catch (Exception E)
            {

            }
        }

        private void CarregarImagensParticipantes()
        {
            try
            {
                BitmapImage gEspecialista01 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\1.jpg"));
                imgEspecialista01.Source = gEspecialista01;

                BitmapImage gEspecialista02 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\2.jpg"));
                imgEspecialista02.Source = gEspecialista02;

                BitmapImage gEspecialista03 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\3.jpg"));
                imgEspecialista03.Source = gEspecialista03;

                BitmapImage gEspecialista04 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\4.jpg"));
                imgEspecialista04.Source = gEspecialista04;

                BitmapImage gEspecialista05 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\5.jpg"));
                imgEspecialista05.Source = gEspecialista05;

                BitmapImage gEspecialista06 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\6.jpg"));
                imgEspecialista06.Source = gEspecialista06;

                BitmapImage gEspecialista07 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\7.jpg"));
                imgEspecialista07.Source = gEspecialista07;

                BitmapImage gEspecialista08 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\8.jpg"));
                imgEspecialista08.Source = gEspecialista08;

                BitmapImage gEspecialista09 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\9.jpg"));
                imgEspecialista09.Source = gEspecialista09;

                BitmapImage gEspecialista10 = new BitmapImage(new Uri(Directory.GetCurrentDirectory() + @"\Especialistas\10.jpg"));
                imgEspecialista10.Source = gEspecialista10;

            }
            catch (Exception E)
            {

            }
        }

        private void btnPermitir_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PermitirDMX();
            }
            catch (Exception E)
            {

            }
        }

        private void btnBloquear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BloquearDMX();
            }
            catch (Exception E)
            {

            }
        }

        private void btnApagar_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ApagarDMX();
            }
            catch (Exception E)
            {

            }
        }

        private void PermitirDMX()
        {
            try
            {
                foreach (int vCanal in gLstCanais)
                {
                    DMXData[vCanal - 1] = Properties.Settings.Default.corPermitida.R;
                    DMXData[vCanal] = Properties.Settings.Default.corPermitida.G;
                    DMXData[vCanal + 1] = Properties.Settings.Default.corPermitida.B;
                }

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();

                btnEspecialista01.Source = gVerde;
                btnEspecialista02.Source = gVerde;
                btnEspecialista03.Source = gVerde;
                btnEspecialista04.Source = gVerde;
                btnEspecialista05.Source = gVerde;
                btnEspecialista06.Source = gVerde;
                btnEspecialista07.Source = gVerde;
                btnEspecialista08.Source = gVerde;
                btnEspecialista09.Source = gVerde;
                btnEspecialista10.Source = gVerde;

                //btnEspecialista01.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista02.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista03.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista04.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista05.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista06.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista07.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista08.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista09.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                //btnEspecialista10.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));

                btnEspecialista01.Tag = "TRUE";

                //btnEspecialista01.Tag = "TRUE";
                btnEspecialista02.Tag = "TRUE";
                btnEspecialista03.Tag = "TRUE";
                btnEspecialista04.Tag = "TRUE";
                btnEspecialista05.Tag = "TRUE";
                btnEspecialista06.Tag = "TRUE";
                btnEspecialista07.Tag = "TRUE";
                btnEspecialista08.Tag = "TRUE";
                btnEspecialista09.Tag = "TRUE";
                btnEspecialista10.Tag = "TRUE";
            }
            catch (Exception E)
            {

            }
        }

        private void Permitir_Individual_DMX(string pCanal)
        {
            try
            {
                int vCanal = Convert.ToInt16(pCanal);

                DMXData[vCanal - 1] = Properties.Settings.Default.corPermitida.R;
                DMXData[vCanal] = Properties.Settings.Default.corPermitida.G;
                DMXData[vCanal + 1] = Properties.Settings.Default.corPermitida.B;

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();
            }
            catch (Exception E)
            {

            }
        }


        private void BloquearDMX()
        {
            try
            {
                foreach (int vCanal in gLstCanais)
                {
                    DMXData[vCanal - 1] = Properties.Settings.Default.corBloqueada.R;
                    DMXData[vCanal] = Properties.Settings.Default.corBloqueada.G;
                    DMXData[vCanal + 1] = Properties.Settings.Default.corBloqueada.B;
                }

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();

                btnEspecialista01.Source = gVermelho;
                btnEspecialista02.Source = gVermelho;
                btnEspecialista03.Source = gVermelho;
                btnEspecialista04.Source = gVermelho;
                btnEspecialista05.Source = gVermelho;
                btnEspecialista06.Source = gVermelho;
                btnEspecialista07.Source = gVermelho;
                btnEspecialista08.Source = gVermelho;
                btnEspecialista09.Source = gVermelho;
                btnEspecialista10.Source = gVermelho;

                //btnEspecialista01.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista02.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista03.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista04.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista05.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista06.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista07.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista08.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista09.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                //btnEspecialista10.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));

                btnEspecialista01.Tag = "FALSE";

                //btnEspecialista01.Tag = "FALSE";
                btnEspecialista02.Tag = "FALSE";
                btnEspecialista03.Tag = "FALSE";
                btnEspecialista04.Tag = "FALSE";
                btnEspecialista05.Tag = "FALSE";
                btnEspecialista06.Tag = "FALSE";
                btnEspecialista07.Tag = "FALSE";
                btnEspecialista08.Tag = "FALSE";
                btnEspecialista09.Tag = "FALSE";
                btnEspecialista10.Tag = "FALSE";
            }
            catch (Exception E)
            {

            }
        }

        private void Bloquear_Individual_DMX(string pCanal)
        {
            try
            {
                int vCanal = Convert.ToInt16(pCanal);

                DMXData[vCanal - 1] = Properties.Settings.Default.corBloqueada.R;
                DMXData[vCanal] = Properties.Settings.Default.corBloqueada.G;
                DMXData[vCanal + 1] = Properties.Settings.Default.corBloqueada.B;

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();
            }
            catch (Exception E)
            {

            }
        }

        private void ApagarDMX()
        {
            try
            {
                foreach (int vCanal in gLstCanais)
                {
                    DMXData[vCanal - 1] = 0;
                    DMXData[vCanal] = 0;
                    DMXData[vCanal + 1] = 0;
                }

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();
            }
            catch (Exception E)
            {

            }
        }

        private void btnEspecialista01_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Button vButton = (Button)sender;

                switch (vButton.Name.ToString())
                {
                    case "btnEspecialista01":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista01);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista01);
                        }
                        break;

                    case "btnEspecialista02":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista02);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista02);
                        }
                        break;

                    case "btnEspecialista03":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista03);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista03);
                        }
                        break;

                    case "btnEspecialista04":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista04);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista04);
                        }
                        break;

                    case "btnEspecialista05":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista05);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista05);
                        }
                        break;

                    case "btnEspecialista06":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista06);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista06);
                        }
                        break;

                    case "btnEspecialista07":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista07);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista07);
                        }
                        break;

                    case "btnEspecialista08":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista08);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista08);
                        }
                        break;

                    case "btnEspecialista09":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista09);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista09);
                        }
                        break;

                    case "btnEspecialista10":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corPermitida.R, Properties.Settings.Default.corPermitida.G, Properties.Settings.Default.corPermitida.B));
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Properties.Settings.Default.corBloqueada.R, Properties.Settings.Default.corBloqueada.G, Properties.Settings.Default.corBloqueada.B));
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                        }
                        break;
                }

                btnSalvar.Focus();
            }
            catch (Exception E)
            {

            }
        }

        private void Window_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.M)
                {
                    WindowState = WindowState.Maximized;
                    WindowStyle = WindowStyle.None;
                }
                else if (e.Key == Key.Escape)
                {
                    WindowState = WindowState.Normal;
                    WindowStyle = WindowStyle.ToolWindow;
                }
            }
            catch (Exception E)
            {

            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                System.Windows.Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception E)
            {

            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                System.Windows.Application.Current.Shutdown();
                Process.GetCurrentProcess().Kill();
            }
            catch (Exception E)
            {

            }
        }

        private void imgBotaoEspecialista01_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                Image vButton = (Image)sender;

                switch (vButton.Name.ToString())
                {
                    case "btnEspecialista01":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista01);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista01);
                        }
                        break;

                    case "btnEspecialista02":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista02);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista02);
                        }
                        break;

                    case "btnEspecialista03":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista03);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista03);
                        }
                        break;

                    case "btnEspecialista04":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista04);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista04);
                        }
                        break;

                    case "btnEspecialista05":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista05);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista05);
                        }
                        break;

                    case "btnEspecialista06":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista06);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista06);
                        }
                        break;

                    case "btnEspecialista07":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista07);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista07);
                        }
                        break;

                    case "btnEspecialista08":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista08);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista08);
                        }
                        break;

                    case "btnEspecialista09":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista09);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista09);
                        }
                        break;

                    case "btnEspecialista10":
                        if (vButton.Tag.ToString() == "FALSE")
                        {
                            vButton.Source = gVerde;
                            vButton.Tag = "TRUE";
                            Permitir_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                        }
                        else if (vButton.Tag.ToString() == "TRUE")
                        {
                            vButton.Source = gVermelho;
                            vButton.Tag = "FALSE";
                            Bloquear_Individual_DMX(Properties.Settings.Default.canalEspecialista10);
                        }
                        break;
                }

                btnSalvar.Focus();
            }
            catch (Exception E)
            {

            }
        }

        private void btnCanalDirecao_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                string vCor = "255";

                if (btnCanalDirecao.Tag.ToString() == "FALSE")
                {
                    btnCanalDirecao.Tag = "TRUE";
                    vCor = "0";

                    btnCanalDirecao.Source = gPreto;
                }
                else
                {
                    btnCanalDirecao.Tag = "FALSE";
                    vCor = "255";

                    btnCanalDirecao.Source = gVermelho;
                }

                int vCanal = Convert.ToInt16(Properties.Settings.Default.canalDirecao);

                DMXData[vCanal - 1] = Convert.ToByte(vCor);
                DMXData[vCanal] = Convert.ToByte("0"); ;
                DMXData[vCanal + 1] = Convert.ToByte("0"); 

                artEngine.Start();
                artEngine.SendDMX(universe, DMXData, DMXData.Length);
                artEngine.Pause();
            }
            catch (Exception E)
            {

            }
        }
    }
}
